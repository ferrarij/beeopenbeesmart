#include <Arduino_MKRENV.h>
#include <MKRWAN.h>
#include "arduino_secrets.h"
#include <DHT.h>
#include "HX711.h"
#include <OneWire.h>
#include <DallasTemperature.h>

#define DHTPIN 0
#define DHTTYPE DHT22 
DHT dht(DHTPIN, DHTTYPE);

#define ONE_WIRE_BUS 1
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

#define CLK   A0
#define DOUT1  A1
#define DOUT2  A2
#define DOUT3  A3
#define DOUT4  A4

HX711 scale1;
HX711 scale2;
HX711 scale3;
HX711 scale4;

LoRaModem modem;

String appEui = SECRET_APP_EUI;
String appKey = SECRET_APP_KEY;

typedef struct __attribute__((__packed__)) {
  int16_t temperature_dht;
  uint8_t humidity_dht;     
  int16_t weight;
  int16_t temperature1;
  int16_t temperature2;
} SensorDataStruct;

SensorDataStruct sensor;

int connected;

void setup() {

  Serial.begin(9600);

  dht.begin();

  sensors.begin();

  scale1.begin(DOUT1, CLK);
  scale1.set_offset(-125465.00);
  scale1.set_scale(-114);

  scale2.begin(DOUT2, CLK);
  scale2.set_offset(-168785.00);
  scale2.set_scale(-115);
    
  scale3.begin(DOUT3, CLK);
  scale3.set_offset(76525.80);
  scale3.set_scale(-113);

  scale4.begin(DOUT4, CLK);
  scale4.set_offset(-140330.72);
  scale4.set_scale(-115);

  if (modem.begin(EU868)) {
    connected = modem.joinOTAA(appEui, appKey);
    if (connected) {
      modem.minPollInterval(1 * 60);
    }
  }
}

void loop() {

    sensors.requestTemperatures(); 

    sensor.temperature_dht = (int16_t)(dht.readTemperature() * 10);
    sensor.humidity_dht = (uint8_t)(dht.readHumidity());
    sensor.weight = uint16_t(-800 + scale1.get_units() + scale2.get_units() + scale3.get_units() + scale4.get_units());
    sensor.temperature1 = (int16_t)(sensors.getTempCByIndex(0)*10);
    sensor.temperature2 = (int16_t)(sensors.getTempCByIndex(1)*10);


  if (connected) {
    modem.beginPacket();
    modem.write((uint8_t *)&sensor, sizeof(sensor));
    int err = modem.endPacket(true);
  }

  delay(60000);
}
